/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import br.com.senac.calculadoradelampada.CalculadoraLampada;
import br.com.senac.calculadoradelampada.Comodo;
import junit.framework.Assert;
import org.junit.Test;

public class TestCalculadoraLampada {

    @Test
    public void deveUtilizarUmaLampadasDe18wComComodoDeUmMetroQuadrados() {

        Comodo comodo = new Comodo(1, 1);

        CalculadoraLampada calculadoraLampada = new CalculadoraLampada();

        int quantidadeLampadas = calculadoraLampada.Cacular(18, comodo);

        Assert.assertEquals(1, quantidadeLampadas);

    }
    
    
    @Test
    public void deveUtilizarUmLampadasDe36wComComodoDeDoisMetroQuadrados() {
        Comodo comodo = new Comodo(2, 1);
        
        CalculadoraLampada calculadoraLampada = new CalculadoraLampada();
        
        int quantidadeLampadas = calculadoraLampada.Cacular(36, comodo);
        
        Assert.assertEquals(1, quantidadeLampadas);
    }

    @Test
    public void deveUtilizarTresLampadasDe18wComComodoDeDoisMetroEMeioQuadrados() {
        Comodo comodo = new Comodo(2.5, 1);
        
        CalculadoraLampada calculadoraLampada = new CalculadoraLampada();
        
        int quantidadeLampadas = calculadoraLampada.Cacular(18, comodo);
        
        Assert.assertEquals(3, quantidadeLampadas);
    }

    @Test
    public void deveUtilizar17LampadasDe18wComComodoDeDezesseeMenosDaMetadeMetrosQuadrados() {
        Comodo comodo = new Comodo(10.5, 1.55);
        
        CalculadoraLampada calculadoraLampada = new CalculadoraLampada();
        
        int quantidadeLampadas = calculadoraLampada.Cacular(18, comodo);
        
        Assert.assertEquals(17, quantidadeLampadas);

    }
    
     @Test
    public void deveUtilizar7LampadasDe45wComComodoDeDezesseeMenosDaMetadeMetrosQuadrados() {
        Comodo comodo = new Comodo(10.5, 1.55);
        
        CalculadoraLampada calculadoraLampada = new CalculadoraLampada();
        
        int quantidadeLampadas = calculadoraLampada.Cacular(45, comodo);
        
        Assert.assertEquals(7, quantidadeLampadas);

    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}
